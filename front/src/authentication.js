const users = [{
        email: "est.vila23@example.com",
        password: "azerty"
    },
    {
        email: "e.lopezguevara@gmail.com",
        password: "azerty"
    }
];

/**
 * @param {String} email
 * @param {String} password
 *
 * @return {?Object}
 */
function login(email, password) {
    return users.find(user => user.email === email && user.password === password) || null;
}

export { login };
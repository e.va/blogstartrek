import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'


Vue.config.productionTip = false


Vue.use(VueRouter);


const router = new VueRouter({
    mode: 'history',
    routes: [{
        path: '/',
        component: require('./components/Bio.vue').default,
        name: 'root'
   
    }, {

        path: '/filmo',
        component: require('./components/Filmo.vue').default,
        name: 'filmo'
    }, {
        path: '/galerie',
        component: require('./components/Galerie.vue').default,
        name: 'galerie'
    }, {

        path: '/livredor',
        component: require('./components/LivreDor.vue').default,
        name: 'livreDor'

    }, ]

})

new Vue({
    el: '#app',
    router,
    render: h => h(App)
})
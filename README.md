# Spocky

[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)  [![forthebadge](http://forthebadge.com/images/badges/powered-by-electricity.svg)](http://forthebadge.com)

Fan site à la mémoire de Leonard Nimoy, principalement connu pour avoir incarné le vulcain Spock dans la franchise Star Trek pendant plus de quarante ans.

### Pré-requis

- Visual Studio Code ou PhpStorm
- Symfony
- MySql

### Installation

1. cd ``front``
2. ``npm install`` 
3. cd ``serve``
4. ``composer install``


## Démarrage

- cd ``front``
- Executez la commande ``npm run serve``
- cd ``serve``
- Executez la commande ``symfony serve``

## Fabriqué avec

* [HTML 5](https://developer.mozilla.org/fr/docs/Web/Guide/HTML/HTML5) - Langage HTML (front-end)
* [CSS 3](https://developer.mozilla.org/fr/docs/Web/CSS) - Langage de feuille de style (front-end)
* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) - Langage de programmation (front-end)
* [Vue.js](https://vuejs.org/) - Framework JavaScript (front-end)
* [PHP 7.4.12](https://www.php.net/) - Langage de programmation (back-end)
* [Symfony 5.1.7](https://symfony.com/) - Framework PHP (back-end)
* [MySql](https://www.mysql.com/fr/) - Système de gestion de bases de données relationnelles (back-end)


## Auteure

* **Estefania Vila** [@e.va](https://gitlab.com/e.va)

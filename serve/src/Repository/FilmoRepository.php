<?php

namespace App\Repository;

use App\Entity\Filmo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Filmo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Filmo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Filmo[]    findAll()
 * @method Filmo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filmo::class);
    }

    // /**
    //  * @return Filmo[] Returns an array of Filmo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Filmo
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\CustomCKFinderAuth;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomCKFinderAuth|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomCKFinderAuth|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomCKFinderAuth[]    findAll()
 * @method CustomCKFinderAuth[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomCKFinderAuthRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomCKFinderAuth::class);
    }

    // /**
    //  * @return CustomCKFinderAuth[] Returns an array of CustomCKFinderAuth objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomCKFinderAuth
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

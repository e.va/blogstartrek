<?php

namespace App\Entity;

use App\Repository\LivreDorRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * LivreDor
 *
 * @ORM\Table(name="livre_dor")
 * @ORM\Entity
 */
class LivreDor
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"livredor"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"livredor"})
     */
    private $message;
    
    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="livreDors")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"livredor", "users"})
    */
    private $Auteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    
    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function __toString(){
        
        return $this->message;
    }

    public function getAuteur(): ?User
    {
        return $this->Auteur;
    }

    public function setAuteur(?User $Auteur): self
    {
        $this->Auteur = $Auteur;

        return $this;
    }

    


}

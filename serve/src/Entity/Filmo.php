<?php

namespace App\Entity;

use App\Repository\FilmoRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Form\Type\VichImageType;


/**
 * Filmo
 *
 * @ORM\Table(name="filmo")
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Filmo
{
    /**
     * @var int
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"filmos"})
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"filmos"})
     */
    private $synopsis;

     /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Groups({"filmos"})
     */
    private $movie_title;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"filmos"})
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="images_directory", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *     
     */
    private $updated;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"filmos"})
     */
    private $image_2;

    /**
     * @Vich\UploadableField(mapping="images_directory", fileNameProperty="image")
     * @var File
     */
    private $imageFile_2;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *     
     */
    private $updated_2;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="filmos")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"filmos"})
     */
    private $Auteur;

   
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSynopsis(): ?string
    {
        return $this->synopsis;
    }

    public function setSynopsis(string $synopsis): self
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    public function getMovieTitle(): ?string
    {
        return $this->movie_title;
    }

    public function setMovieTitle(string $movie_title): self
    {
        $this->movie_title = $movie_title;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile(): ?string
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     */
    public function setImageFile(?File $imageFile = null)
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updated = new \Datetime();

        }
    }

    public function getImage2(): ?string
    {
        return $this->image_2;
    }

    public function setImage2(?string $image_2): self
    {
        $this->image_2 = $image_2;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile2(): ?string
    {
        return $this->imageFile_2;
    }

    /**
     * @param File|null $imageFile_2
     */
    public function setImageFile2(?File $imageFile_2 = null)
    {
        $this->imageFile_2 = $imageFile_2;

        if (null !== $imageFile_2) {
            $this->updated = new \Datetime();

        }
    }

    public function getAuteur(): ?User
    {
        return $this->Auteur;
    }

    public function setAuteur(?User $Auteur): self
    {
        $this->Auteur = $Auteur;

        return $this;
    }

}

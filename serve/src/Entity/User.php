<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"users"}) 
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"users", "livredor"}) 
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"users"}) 
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"users"}) 
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Bio::class, mappedBy="Auteur")
     */
    private $bios;

    /**
     * @ORM\OneToMany(targetEntity=Filmo::class, mappedBy="Auteur")
     */
    private $filmos;

    /**
     * @ORM\OneToMany(targetEntity=Galerie::class, mappedBy="Auteur")
     */
    private $galeries;

    /**
     * @ORM\OneToMany(targetEntity=LivreDor::class, mappedBy="Auteur")
     */
    private $livreDors;

    public function __construct()
    {
        $this->bios = new ArrayCollection();
        $this->filmos = new ArrayCollection();
        $this->galeries = new ArrayCollection();
        $this->livreDors = new ArrayCollection();
    }


    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
    public function __toString(){
        
        return $this->email;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Bio[]
     */
    public function getBios(): Collection
    {
        return $this->bios;
    }

    public function addBio(Bio $bio): self
    {
        if (!$this->bios->contains($bio)) {
            $this->bios[] = $bio;
            $bio->setAuteur($this);
        }

        return $this;
    }

    public function removeBio(Bio $bio): self
    {
        if ($this->bios->contains($bio)) {
            $this->bios->removeElement($bio);
            // set the owning side to null (unless already changed)
            if ($bio->getAuteur() === $this) {
                $bio->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Filmo[]
     */
    public function getFilmos(): Collection
    {
        return $this->filmos;
    }

    public function addFilmo(Filmo $filmo): self
    {
        if (!$this->filmos->contains($filmo)) {
            $this->filmos[] = $filmo;
            $filmo->setAuteur($this);
        }

        return $this;
    }

    public function removeFilmo(Filmo $filmo): self
    {
        if ($this->filmos->contains($filmo)) {
            $this->filmos->removeElement($filmo);
            // set the owning side to null (unless already changed)
            if ($filmo->getAuteur() === $this) {
                $filmo->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Galerie[]
     */
    public function getGaleries(): Collection
    {
        return $this->galeries;
    }

    public function addGalery(Galerie $galery): self
    {
        if (!$this->galeries->contains($galery)) {
            $this->galeries[] = $galery;
            $galery->setAuteur($this);
        }

        return $this;
    }

    public function removeGalery(Galerie $galery): self
    {
        if ($this->galeries->contains($galery)) {
            $this->galeries->removeElement($galery);
            // set the owning side to null (unless already changed)
            if ($galery->getAuteur() === $this) {
                $galery->setAuteur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LivreDor[]
     */
    public function getLivreDors(): Collection
    {
        return $this->livreDors;
    }

    public function addLivreDor(LivreDor $livreDor): self
    {
        if (!$this->livreDors->contains($livreDor)) {
            $this->livreDors[] = $livreDor;
            $livreDor->setAuteur($this);
        }

        return $this;
    }

    public function removeLivreDor(LivreDor $livreDor): self
    {
        if ($this->livreDors->contains($livreDor)) {
            $this->livreDors->removeElement($livreDor);
            // set the owning side to null (unless already changed)
            if ($livreDor->getAuteur() === $this) {
                $livreDor->setAuteur(null);
            }
        }

        return $this;
    }

    
}

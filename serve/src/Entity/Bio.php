<?php

namespace App\Entity;

use App\Repository\BioRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Form\Type\VichImageType;


/**
 * Bio
 *
 * @ORM\Table(name="bio")
 * @ORM\Entity
 * @Vich\Uploadable
 * 
 */
class Bio
{
    /**
     * @var int
     * 
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"bio"})
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Groups({"bio"})
     */
    private $title;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"bio"})
     */
    private $intro;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Groups({"bio"})
     */
    private $sous_titre_1;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"bio"})
     */
    private $paragraphe_1;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Groups({"bio"})
     */
    private $sous_titre_2;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"bio"})
     */
    private $paragraphe_2;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"bio"})
     */
    private $paragraphe_3;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"bio"})
     */
    private $paragraphe_4;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255)
     * @Groups({"bio"})
     */
    private $sous_titre_3;

    /**
     * @var string
     * 
     * @ORM\Column(type="text")
     * @Groups({"bio"})
     */
    private $paragraphe_5;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"bio"})
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="images_directory", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     *     
     */
    private $updated;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"bio"})
     */
    private $image_1;

    /**
     * @Vich\UploadableField(mapping="images_directory", fileNameProperty="image")
     * @var File
     */
    private $imageFile_1;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     *     
     */
    private $updated_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"bio"})
     */
    private $image_2;

    /**
     * @Vich\UploadableField(mapping="images_directory", fileNameProperty="image")
     * @var File
     */
    private $imageFile_2;

     /**
     * @ORM\Column(type="datetime", nullable=true)
     *     
     */
    private $updated_2;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bios")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"bio"})
     */
    private $Auteur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIntro(): ?string
    {
        return $this->intro;
    }

    public function setIntro(string $intro): self
    {
        $this->intro = $intro;

        return $this;
    }

    public function getSousTitre1(): ?string
    {
        return $this->sous_titre_1;
    }

    public function setSousTitre1(string $sous_titre_1): self
    {
        $this->sous_titre_1 = $sous_titre_1;

        return $this;
    }

    public function getParagraphe1(): ?string
    {
        return $this->paragraphe_1;
    }

    public function setParagraphe1(string $paragraphe_1): self
    {
        $this->paragraphe_1 = $paragraphe_1;

        return $this;
    }

    public function getSousTitre2(): ?string
    {
        return $this->sous_titre_2;
    }

    public function setSousTitre2(string $sous_titre_2): self
    {
        $this->sous_titre_2 = $sous_titre_2;

        return $this;
    }

    public function getParagraphe2(): ?string
    {
        return $this->paragraphe_2;
    }

    public function setParagraphe2(string $paragraphe_2): self
    {
        $this->paragraphe_2 = $paragraphe_2;

        return $this;
    }

    public function getParagraphe3(): ?string
    {
        return $this->paragraphe_3;
    }

    public function setParagraphe3(string $paragraphe_3): self
    {
        $this->paragraphe_3 = $paragraphe_3;

        return $this;
    }

    public function getParagraphe4(): ?string
    {
        return $this->paragraphe_4;
    }

    public function setParagraphe4(string $paragraphe_4): self
    {
        $this->paragraphe_4 = $paragraphe_4;

        return $this;
    }

    public function getSousTitre3(): ?string
    {
        return $this->sous_titre_3;
    }

    public function setSousTitre3(string $sous_titre_3): self
    {
        $this->sous_titre_3 = $sous_titre_3;

        return $this;
    }

    public function getParagraphe5(): ?string
    {
        return $this->paragraphe_5;
    }

    public function setParagraphe5(string $paragraphe_5): self
    {
        $this->paragraphe_5 = $paragraphe_5;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }
    
    /**
     * @return File|null
     */
    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    /**
     * @param File|null $imageFile
     */
    public function setImageFile(?File $imageFile = null)
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updated = new \Datetime();

        }
    }

    public function getImage1(): ?string
    {
        return $this->image_1;
    }

    public function setImage1(?string $image_1): self
    {
        $this->image_1 = $image_1;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile1(): ?File
    {
        return $this->imageFile_1;
    }

    /**
     * @param File|null $imageFile_1
     */
    public function setImageFile1(?File $imageFile_1 = null)
    {
        $this->imageFile_1 = $imageFile_1;

        if (null !== $imageFile_1) {
            $this->updated_1 = new \Datetime();

        }
    }

    public function getImage2(): ?string
    {
        return $this->image_2;
    }

    public function setImage2(?string $image_2): self
    {
        $this->image_2 = $image_2;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile2(): ?File
    {
        return $this->imageFile_2;
    }

    /**
     * @param File|null $imageFile_2
     */
    public function setImageFile2(?File $imageFile_2 = null)
    {
        $this->imageFile_2 = $imageFile_2;

        if (null !== $imageFile_2) {
            $this->updated_2 = new \Datetime();

        }
    }

    public function getAuteur(): ?User
    {
        return $this->Auteur;
    }

    public function setAuteur(?User $Auteur): self
    {
        $this->Auteur = $Auteur;

        return $this;
    }
}

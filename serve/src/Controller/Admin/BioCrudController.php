<?php

namespace App\Controller\Admin;

use App\Entity\Bio;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;




class BioCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Bio::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title', 'Title'),
            TextEditorField::new('intro', 'Intro'),
            TextField::new('sous_titre_1', 'Sous Titre 1'),
            TextEditorField::new('paragraphe_1', 'Paragraphe 1'),
            TextField::new('sous_titre_2', 'Sous Titre 2'),
            TextEditorField::new('paragraphe_2', 'Paragraphe 2'),
            TextEditorField::new('paragraphe_3', 'Paragraphe 3'),
            TextEditorField::new('paragraphe_4', 'Paragraphe 4'),
            TextField::new('sous_titre_3', 'Sous Titre 3'),
            TextEditorField::new('paragraphe_5', 'Paragraphe 5'),
            ImageField::new('image')->setBasePath($this->getParameter('app.path.images_directory'))->onlyOnIndex(),
            VichImageField::new('imageFile')->hideOnIndex(),
            ImageField::new('image_1')->setBasePath($this->getParameter('app.path.images_directory'))->onlyOnIndex(),
            VichImageField::new('imageFile_1')->hideOnIndex(),
            ImageField::new('image_2')->setBasePath($this->getParameter('app.path.images_directory'))->onlyOnIndex(),
            VichImageField::new('imageFile_2')->hideOnIndex(),
            AssociationField::new('Auteur', 'Auteur'),
                                  
        ];
    }
    
}

<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Entity\LivreDor;
use App\Entity\Message;
use App\Entity\Bio;
use App\Entity\Filmo;
use App\Entity\Galerie;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('FanSiteSpocky');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Bio', 'fas fa-hand-spock', Bio::class);
        yield MenuItem::linkToCrud('Filmo', 'fas fa-film', Filmo::class);
        yield MenuItem::linkToCrud('Galerie', 'fas fa-images', Galerie::class);
        yield MenuItem::linkToCrud('LivreDor', 'fas fa-book', LivreDor::class);
        yield MenuItem::linkToCrud('Message', 'fas fa-pencil-alt', Message::class);
        yield MenuItem::linkToCrud('User', 'fas fa-users-cog', User::class);

    }
}

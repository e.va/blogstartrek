<?php

namespace App\Controller\Admin;

use App\Entity\Filmo;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class FilmoCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Filmo::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('movie_title', 'Movie Title'),
            TextEditorField::new('synopsis', 'Synopsis'),
            ImageField::new('image')->setBasePath($this->getParameter('app.path.images_directory'))->onlyOnIndex(),
            VichImageField::new('imageFile')->hideOnIndex(),
            ImageField::new('image_2')->setBasePath($this->getParameter('app.path.images_directory'))->onlyOnIndex(),
            VichImageField::new('imageFile_2')->hideOnIndex(),
            AssociationField::new('Auteur', 'Auteur'),
            
        ];
    }
    
}

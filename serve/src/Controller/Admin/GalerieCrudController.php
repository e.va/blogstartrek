<?php

namespace App\Controller\Admin;

use App\Entity\Galerie;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class GalerieCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Galerie::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            ImageField::new('image')->setBasePath($this->getParameter('app.path.images_directory'))->onlyOnIndex(),
            VichImageField::new('imageFile')->hideOnIndex(),
            AssociationField::new('Auteur', 'Auteur'),
        ];
    }
    
}

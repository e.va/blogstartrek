<?php

namespace App\Controller\Admin;

use App\Entity\LivreDor;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class LivreDorCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return LivreDor::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('Auteur', 'Auteur'),
            TextEditorField::new('message', 'Message'),            

        ];
    }
}

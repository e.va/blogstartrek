<?php

namespace App\Controller\Api;

use App\Entity\LivreDor;
use App\Repository\LivreDorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiLivreDorController extends AbstractController
{
    /**
     * @Route("/api/livredor", name="api_livredor", methods={"GET"})
     */
    public function getLivreDors(LivreDorRepository $livredorRepository, SerializerInterface $serializer): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $livredor = $livredorRepository->findAll();

        $response = $this->json($livredor, 200, [], ['groups'=>"livredor"]);

        return $response;
    }

    /**
     * @Route("/api/livredor", name="api_new_livredor", methods={"POST"})
     */
    public function createLivreDor(Request $request, EntityManagerInterface $entityManager)
    {
        // récupère les données du LivreDor
        $livredor = json_decode($request->getContent(), true);
        // créé un nouvel objet LivreDor avec les données récupérées
        $livredor = new Livredor ();
        $livredor->setMessage($content['message']);
        $livredor->setAuteur($content['author']);        
               
        

        // prépare + insère les données dans la BDD
        $entityManager->persist($livredor);
        $entityManager->flush();

        return $this->json($livredor->getId(), 201);
    }
}

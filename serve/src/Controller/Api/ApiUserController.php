<?php

namespace App\Controller\Api;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiUserController extends AbstractController
{
    /**
     * @Route("/api/user", name="api_user", methods={"GET"})
     */
    public function getUsers(UserRepository $userRepository, SerializerInterface $serializer): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $users = $userRepository->findAll();

        $response = $this->json($users, 200, [], ['groups'=>"users"]);

        return $response;
    }

    /**
     * @Route("/api/user", name="api_new_user", methods={"POST"})
     */
    public function createUser(Request $request, EntityManagerInterface $entityManager)
    {
        // récupère les données de l'utilisateur
        $userr = json_decode($request->getContent(), true);

        // créé un nouvel objet Utilisateur avec les données récupérées
        $user = new User ();
        $user->setEmail($content['email']);
        $user->setRoles($content['roles']);
        $user->setPassword($content['password']);

        // prépare + insère le nouveau utilisateur dans la BDD
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json($user->getId(), 201);
    }
}

<?php

namespace App\Controller\Api;

use App\Entity\Bio;
use App\Repository\BioRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiBioController extends AbstractController
{
    /**
     * @Route("/api/bio", name="api_bio", methods={"GET"})
     */
    public function getBio(BioRepository $bioRepository, SerializerInterface $serializer): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $bio = $bioRepository->findAll();

        $response = $this->json($bio, 200, [], ['groups'=>"bio"]);

        return $response;
    }

    /**
     * @Route("/api/bio", name="api_new_bio", methods={"POST"})
     */
    public function createBio(Request $request, EntityManagerInterface $entityManager)
    {
        // récupère les données de la bio envoyée
        $bio = json_decode($request->getContent(), true);

        // créé un nouvel objet Bio avec les données récupérées
        $bio = new Bio ();
        $bio->setTitle($content['title']);
        $bio->setIntro($content['intro']);
        $bio->setSousTitre1($content['sous_titre_1']);
        $bio->setParagraphe1($content['paragraphe_1']);
        $bio->setSousTitre2($content['sous_titre_2']);
        $bio->setParagraphe2($content['paragraphe_2']);
        $bio->setParagraphe3($content['paragraphe_3']);
        $bio->setParagraphe4($content['paragraphe_4']);
        $bio->setSousTitre3($content['sous_titre_3']);
        $bio->setParagraphe5($content['paragraphe_5']);
        $bio->setImage($content['image']);
        $bio->setImage1($content['image_1']);
        $bio->setImage2($content['image_2']);
        $bio->setAuteur($content['Auteur']);
        

        // prépare + insère la nouvelle bio dans la BDD
        $entityManager->persist($bio);
        $entityManager->flush();

        return $this->json($bio->getId(), 201);
    }
}
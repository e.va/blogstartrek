<?php

namespace App\Controller\Api;

use App\Entity\Filmo;
use App\Repository\FilmoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiFilmoController extends AbstractController
{
    /**
     * @Route("/api/filmo", name="api_filmo", methods={"GET"})
     */
    public function getFilmo(FilmoRepository $filmoRepository, SerializerInterface $serializer): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $filmo = $filmoRepository->findAll();

        $response = $this->json($filmo, 200, [], ['groups'=>"filmos"]);

        return $response;
    }

    /**
     * @Route("/api/filmo", name="api_new_filmo", methods={"POST"})
     */
    public function createFilmo(Request $request, EntityManagerInterface $entityManager)
    {
        // récupère les données de la filmo envoyée
        $filmo = json_decode($request->getContent(), true);

        // créé un nouvel objet Filmo avec les données récupérées
        $filmo = new Filmo ();
        $filmo->setSynopsis ($content['synopsis']);
        $filmo->setMovieTitle ($content['movie_title']);
        $filmo->setImage ($content['image']);
        $filmo->setImage2 ($content['image_2']);
        $filmo->setAuteur($content['Auteur']);

        // prépare + insère la nouvelle filmo dans la BDD
        $entityManager->persist($filmo);
        $entityManager->flush();

        return $this->json($filmo->getId(), 201);
    }
}
<?php

namespace App\Controller\Api;

use App\Entity\Message;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiMessageController extends AbstractController
{
    /**
     * @Route("/api/message", name="api_message", methods={"GET"})
     */
    public function getMessages(MessageRepository $messageRepository, SerializerInterface $serializer): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $message = $messageRepository->findAll();

        $response = $this->json($message, 200, [], ['groups'=>"messages"]);

        return $response;
    }

    /**
     * @Route("/api/message", name="api_new_message", methods={"POST"})
     */
    public function createMessage(Request $request, EntityManagerInterface $entityManager)
    {
        // récupère les données
        $data = json_decode($request->getContent(), true);
        // créé un nouvel objet Message avec les données récupérées
        $message = new Message ();
        $message->setMessage($data['message']);
        $message->setEmail($data['email']);        
               
        

        // prépare + insère les données dans la BDD
        $entityManager->persist($message);
        $entityManager->flush();

        return $this->json($message->getId(), 201);
    }
}

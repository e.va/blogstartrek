<?php

namespace App\Controller\Api;

use App\Entity\Galerie;
use App\Repository\GalerieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiGalerieController extends AbstractController
{
    /**
     * @Route("/api/galerie", name="api_galerie", methods={"GET"})
     */
    public function getGalerie(GalerieRepository $galerieRepository, SerializerInterface $serializer): Response
    {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $galerie = $galerieRepository->findAll();

        $response = $this->json($galerie, 200, [], ['groups'=>"galerie"]);

        return $response;
    }

    /**
     * @Route("/api/galerie", name="api_new_galerie", methods={"POST"})
     */
    public function createGalerie(Request $request, EntityManagerInterface $entityManager)
    {
        // récupère les données de l'image envoyée
        $galerie = json_decode($request->getContent(), true);

        // créé un nouvel objet Galerie avec les données récupérées
        $galerie = new Galerie ();
        $galerie->setImage ($content['image']);
        $galerie->setImage2 ($content['image_2']);
        $galerie->setAuteur($content['Auteur']);

        // prépare + insère la nouvelle image dans la BDD
        $entityManager->persist($galerie);
        $entityManager->flush();

        return $this->json($galerie->getId(), 201);
    }
}
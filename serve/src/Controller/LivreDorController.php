<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class LivreDorController extends AbstractController
{
    /**
     * @Route("/livre/dor", name="livre_dor")
     */
    public function index()
    {
        return $this->render('livre_dor/index.html.twig', [
            'controller_name' => 'LivreDorController',
        ]);
    }
}

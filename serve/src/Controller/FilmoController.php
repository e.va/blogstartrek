<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FilmoController extends AbstractController
{
    /**
     * @Route("/filmo", name="filmo")
     */
    public function index()
    {
        return $this->render('filmo/index.html.twig', [
            'controller_name' => 'FilmoController',
        ]);
    }
}
